FROM omneedia/ci

ENV HTTP_PROXY=

RUN mkdir /srv/app && chown 1000:1000 -R /srv/app

WORKDIR /srv/app

COPY app .

ENTRYPOINT oa config set proxy $HTTP_PROXY && oa install && oa start
