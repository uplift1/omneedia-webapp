module.exports = {
  world: function (str, cb) {
    var moment = this.using("moment");
    moment.locale(this.i18n);
    this._("greetings", function (o) {
      cb(o + " " + moment().format("LLLL"));
    });
  },
};
